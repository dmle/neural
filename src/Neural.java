import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.*;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.TransferFunctionType;

import javax.sound.sampled.*;
import java.io.*;
import java.net.URISyntaxException;
import java.util.*;

public class Neural {
    int neuronCount;
    int maxIteration;
    static String nnName;
    String path;

    Neural() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Формат дерева директорий:");
        System.out.println("RootDir");
        System.out.println(" Rock");
        System.out.println("  song1.mp3");
        System.out.println("  song2.mp3");
        System.out.println(" Pop");
        System.out.println("  song3.mp3");
        System.out.println("  song4.mp3");
        System.out.println(" Classic");
        System.out.println("  song5.mp3");
        System.out.println("  song6.mp3");
        System.out.println("Укажите путь до выборки:");
        path = scanner.nextLine();
        System.out.println("Выберите нейросеть:");
        System.out.println("1. Adaline;\n" +
                        //   "2. AutoencoderNetwork;\n" +
                        "2. BAM;\n" +
                        "3. CompetitiveNetwork;\n" +
                        // "4. ConvolutionalNetwork;\n" +
                        //  "6. ElmanNetwork;\n" +
                        //  "7. JordanNetwork;\n" +
                        //"5. Hopfield;\n" +
                        //   "9. Instar;\n" +
                        "4. Kohonen;\n" +
                        // "7. MaxNet;\n" +
                        // "12. MultiLayerPerceptron;\n" +
                        //  "13. NeuroFuzzyPerceptron;\n" +
                        // "8. Outstar;\n" +
                        "5. Perceptron.\n"
                //  "16. RBFNetwork;\n" +
                //  "17. SupervisedHebbianNetwork;\n" +
                //  "18. UnsupervisedHebbianNetwork."
        );
        nnName = scanner.nextLine();
        System.out.println("Введите количество нейронов.");
        neuronCount = Integer.parseInt(scanner.nextLine());
        System.out.println("Введите количество итераций обучения.");
        maxIteration = Integer.parseInt(scanner.nextLine());
    }

    private double[] getDoubleArrayFromMP3(String path) throws IOException {
        byte[] byteArray = inputStreamToByteArray(
                new FileInputStream(new File(path)));

        double[] doubleArray = new double[neuronCount];
        int part = byteArray.length / neuronCount;
        for (int i1 = 0; i1 < neuronCount; i1++) {
            int[] partArray = new int[part + 1];
            for (int i = i1 * part; i < byteArray.length - 1; i++) {
                partArray[i - i1 * part] = byteArray[i];
                if (i == (i1 + 1) * part) {
                    int[] maxValuesArray = new int[1000];
                    int[] minValuesArray = new int[10];
                    int[] medium = new int[10];
                    for (int s1 = 0; s1 < 1000; s1++) {
                        int[] newPartArray = Arrays.copyOfRange(partArray, s1 * partArray.length / 1000, (s1 + 1) * partArray.length / 1000);
                        maxValuesArray[s1] = maxValue(newPartArray);///newPartArray.length;
                        //minValuesArray[s1] = minValue(newPartArray);
                        //medium[s1] = maxValuesArray[s1]+minValuesArray[s1];
                    }
                    double d = 0;
                    for (int s = 0; s < maxValuesArray.length; s++) {
                        d = d + maxValuesArray[s];
                    }
                    doubleArray[i1] = d / 1000;
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(doubleArray));
        return doubleArray;

// converting byteArray to intArray
        //for (int i = 0; i < 50; doubleArray[i] = byteArray[i++]) ;
        //return doubleArray;
        //System.out.println(Arrays.toString(doubleArray));
    }

    private int maxValue(int[] chars) {
        int max = 0;
        for (int ktr = 0; ktr < chars.length; ktr++) {
            if (chars[ktr] > max) {
                max = chars[ktr];
            }
        }
        return max;
    }

    private int minValue(int[] chars) {
        int min = 0;
        for (int ktr = 0; ktr < chars.length; ktr++) {
            if (chars[ktr] < min) {
                min = chars[ktr];
            }
        }
        return min;
    }

    public byte[] inputStreamToByteArray(InputStream inStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[8192];
        int bytesRead;
        while ((bytesRead = inStream.read(buffer)) > 0) {
            baos.write(buffer, 0, bytesRead);
        }
        return baos.toByteArray();
    }

    private File[] getPathsToMusicFolders() throws IOException {
        File file = null;
        file = new File(path);
        return file.listFiles();
    }

    private List<File> getMusicFilesFromFolders(List<File> files, File dir) {
        for (File child : dir.listFiles()) {
            if (child.isDirectory()) {
                getMusicFilesFromFolders(files, child);
            } else if (child.isFile() && child.getName().endsWith(".mp3")) {
                files.add(child);
            }
        }
        return files;
    }

    public static void main(String[] args) throws IOException, UnsupportedAudioFileException, URISyntaxException {
        // TODO Auto-generated method stub
        Neural neural = new Neural();
//training
        DataSet trainingSet =
                new DataSet(neural.neuronCount, 6);

        List<File> testing = new LinkedList<>();
        List<File> training1 = new LinkedList<>();
        List<Integer> result = new LinkedList<>();
        int i = 0;
        for (File file : neural.getPathsToMusicFolders()) {
            List<File> files1 = neural.getMusicFilesFromFolders(new LinkedList<>(), file);
            List<File> training = new LinkedList<>();
            for (int a = 0; a < files1.size() / 2; a++) {
                training.add(files1.get(a));
            }
            files1.removeAll(training);
            testing.addAll(files1);
            for (int a1 = 0; a1 < files1.size(); a1++) {
                result.add(i);
            }
            training1.addAll(training);
            for (File file1 : training) {
                double[] output = new double[6];
                output[i] = 1.0;
                System.out.println(file1.getName());
                System.out.println(Arrays.toString(output));
                //create training set
                trainingSet.addRow(new DataSetRow(neural.getDoubleArrayFromMP3(file1.getPath()),
                        output));
            }
            i++;
        }

        NeuralNetwork nn = null;
        if (nnName.equals("1")) {
            startTestingNN(neural, new Adaline(neural.neuronCount), trainingSet, testing, result);
        }
        // nn = new AutoencoderNetwork(neural.neuronCount, hiddenNeurons);
        if (nnName.equals("2")) {
            startTestingNN(neural, new BAM(neural.neuronCount, 6), trainingSet, testing, result);
        }
        if (nnName.equals("3")) {
            startTestingNN(neural, new CompetitiveNetwork(neural.neuronCount, 6), trainingSet, testing, result);
        }
       /* if (nnName.equals("4")) {
            startTestingNN(neural, new ConvolutionalNetwork(), trainingSet, testing, result);
        }*/
        // nn = new ElmanNetwork(neural.neuronCount,hiddenNeurons, contextNeurons, 6);
        //nn = new JordanNetwork(neural.neuronCount,hiddenNeurons, contextNeurons, 6);
        /*if (nnName.equals("5")) {
            startTestingNN(neural, new Hopfield(neural.neuronCount), trainingSet, testing, result);
        }*/
        //nn = new Instar();
        if (nnName.equals("4")) {
            startTestingNN(neural, new Kohonen(neural.neuronCount, 6), trainingSet, testing, result);
        }
        if (nnName.equals("7")) {
            startTestingNN(neural, new MaxNet(neural.neuronCount), trainingSet, testing, result);
        }
        // nn = new MultiLayerPerceptron();
        //nn = new NeuroFuzzyPerceptron();
        if (nnName.equals("8")) {
            startTestingNN(neural, new Outstar(6), trainingSet, testing, result);
        }
        if (nnName.equals("5")) {
            for (TransferFunctionType tp : TransferFunctionType.values()) {
                if (tp.equals(TransferFunctionType.TANH) || tp.equals(TransferFunctionType.GAUSSIAN) || tp.equals(TransferFunctionType.SGN) || tp.equals(TransferFunctionType.LINEAR) || tp.equals(TransferFunctionType.LOG) || tp.equals(TransferFunctionType.TRAPEZOID) || tp.equals(TransferFunctionType.RAMP)) {
                    continue;
                }
                System.out.println(tp.name());
                startTestingNN(neural, new Perceptron(neural.neuronCount, 6, tp), trainingSet, testing, result);
            }
        }
        // nn = new RBFNetwork();
        //nn = new SupervisedHebbianNetwork();
        //nn = new UnsupervisedHebbianNetwork();
    }

    static void startTestingNN(Neural neural, NeuralNetwork nn, DataSet trainingSet, List<File> testing, List<Integer> result) throws IOException {
        StringBuffer sb = new StringBuffer();
        BackPropagation backPropagation = new BackPropagation();
        backPropagation.setMaxIterations(neural.maxIteration);
        NeuralNetwork nNetwork = nn;//new Perceptron(neural.neuronCount, 6, tp);
         /*   Layer outputLayer = new Layer();
            for(int out=0; out<6; out++) {
                outputLayer.addNeuron(new Neuron());
            }
            Layer hiddenLayerTwo = new Layer();
            for(int out=0; out<100; out++) {
                hiddenLayerTwo.addNeuron(new Neuron());
            }
            Layer hiddenLayerOne  = new Layer();
            for(int out=0; out<100; out++) {
                hiddenLayerOne .addNeuron(new Neuron());
            }
            Layer inputLayer  = new Layer();
            for(int out=0; out<50; out++) {
                inputLayer .addNeuron(new Neuron());
            }
            nNetwork.addLayer(0, inputLayer);
            nNetwork.addLayer(1, hiddenLayerOne);
            ConnectionFactory.fullConnect(nNetwork.getLayerAt(0), nNetwork.getLayerAt(1));
            nNetwork.addLayer(2, hiddenLayerTwo);
            ConnectionFactory.fullConnect(nNetwork.getLayerAt(1), nNetwork.getLayerAt(2));
            nNetwork.addLayer(3, outputLayer);
            ConnectionFactory.fullConnect(nNetwork.getLayerAt(2), nNetwork.getLayerAt(3));
            ConnectionFactory.fullConnect(nNetwork.getLayerAt(0),
                    nNetwork.getLayerAt(nNetwork.getLayersCount()-1), false);
            nNetwork.setInputNeurons(inputLayer.getNeurons());
            nNetwork.setOutputNeurons(outputLayer.getNeurons());*/


        // sb.append(tp + "\n");
        Date now = new Date();
        // System.out.println(tp.name() + ": startTestingNN learning time - " + now);
        // learn the training set
        nNetwork.learn(trainingSet, backPropagation);
        System.out.println("The time has passed: from " + now + " to " + new Date());
        long res1 = new Date().getTime() - now.getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(res1);

        int mMinute = calendar.get(Calendar.MINUTE);
        int mSecond = calendar.get(Calendar.SECOND);

        sb.append("Time for learning: " + mMinute + "m:" + mSecond + "s\n");
        // learn the training set
        // save the trained network into file
        nNetwork.save("or_perceptron.nnet");
        System.out.println("end");

        int next = 0;
        double sumTrue = 0;
        for (File file : testing) {
            // set network input
            nNetwork.setInput(neural.getDoubleArrayFromMP3(file.getPath()));
            nNetwork.calculate();
            double[] networkOutput = nNetwork.getOutput();
            System.out.println(Arrays.toString(networkOutput));
            for (int i2 = 0; i2 < networkOutput.length; i2++) {
                if (networkOutput[i2] > 0) {
                    System.out.println(file.getName());
                    // System.out.println("*result:" + getPathsToMusicFolders()[i2].getName());
                    //System.out.println("*expected:" + getPathsToMusicFolders()[result.get(next)].getName());
                    if (neural.getPathsToMusicFolders()[i2].equals(neural.getPathsToMusicFolders()[result.get(next)])) {
                        sumTrue++;
                    }
                    break;
                }
            }
            next++;
        }
        System.out.println("Result: " + sumTrue / testing.size());
        sb.append(sumTrue / testing.size() + "\n");
        System.out.println(sb);
    }
}